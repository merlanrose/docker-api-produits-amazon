#Pour gerer par la suite d'autres types
type = "Produit"

def get_produit_source_fils_Neo4j(id, GRAPH_DATABASE):
    req = "MATCH (source:" + type + ")-[relation]->(destination:" + type + ") where source.id='" + id + \
           "' RETURN  " \
           "source.id " \
           "as source , " \
           "relation.type as link_type, " \
           "relation.value as value, " \
           "destination.id as target "
    list = GRAPH_DATABASE.data(req)
    return list

# chemin le plus court entre 2 noeuds
def get_shortest_path_Neo4j(idS, idD, GRAPH_DATABASE):
    req = "match p=shortestPath((source:" + type + ")-[*]->(destination:" + type + ")) where source.id='" + idS + "' and " \
                                                                                                                   "destination.id='" + idD + \
           "' RETURN length(p) as distance"
    distance = GRAPH_DATABASE.data(req)
    return distance

# recupere les noeuds lies au derniers noeuds connus
def get_produit_niveau_arbre_Neo4j(idS, dist, GRAPH_DATABASE):
    req = "match (noeudsource:" + type + ") - [*" + str(
        dist) + "]->(source:" + type + ") - [relation]->(destination:" + type + ") where " \
                                                                                "noeudsource.id='" + idS + "' return  " \
                                                                                                           "source.id " \
                                                                                                           "as source " \
                                                                                                           ", " \
                                                                                                           "relation.type as link_type, " \
                                                                                                           "relation.value as value, " \
                                                                                                           "destination.id as target "
    relations = GRAPH_DATABASE.data(req)
    return relations
