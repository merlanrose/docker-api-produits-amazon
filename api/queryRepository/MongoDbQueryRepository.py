def get_mongo_categories(collection):
    categorie = collection.distinct("categories")
    return categorie

def  get_mongo_produits_recherche(collection,numpage,nbelement, categorie,title,pricemin,pricemax):
    page = numpage - 1
    nbel = nbelement
    count=0
    produits = None

    if pricemax==None:
        var = collection.find({}, {"price": 1}).sort([("price", -1)]).limit(1)
        for v in var:
            pricemax= v['price']


    prixmax= collection.find({},{"price":1}).sort([("price", -1)]).limit(1)
    prixmaxresult = 0
    for p in prixmax:
        prixmaxresult = p['price']

    if (categorie=='""' and title=='""'):
        count=collection.find({"price": { "$gt" :  pricemin, "$lt" : pricemax}},{"_id":0}).count()
        #produits = collection.find({"price": { "$gt" :  pricemin, "$lt" : pricemax}},{"_id":0,"reviews":0})[page * nbel: page * nbel + nbel].sort("asin", 1)
        produits = collection.aggregate([
            {"$match":
                {
                    "price": {"$gt": pricemin, "$lt": pricemax}
                }
            },
            {"$project":
                 {"_id": 0,
                  "asin": 1,
                  "groupe": {"$ifNull": [1, 1]},
                  "title": {"$ifNull": ["$title", "undefined"]},
                  "description": {"$ifNull": ["$description", "undefined"]},
                  "price": {"$ifNull": ["$price", "undefined"]},
                  "brand": {"$ifNull": ["$brand", "undefined"]},
                  "categories": {"$ifNull": ["$categories", "undefined"]},
                  "salesRank": {"$ifNull": ["$salesRank", "undefined"]},
                  "imUrl": {"$ifNull": ["$imUrl", "undefined"]}
                  }
             },
            {"$limit": page * nbel + nbel},
            {"$skip": page * nbel},
            {"$sort" :{"asin" :1}}
        ])

    elif  (categorie!='""' and title=='""'):
        #prixmax = collection.find({"categories": categorie},{"price":1}).sort([("price", -1)]).limit(1)
        count=collection.find({"categories": categorie, "price": { "$gt" :  pricemin, "$lt" : pricemax}},{"_id":0}).count()
        #produits = collection.find({"categories": categorie, "price": { "$gt" :  pricemin, "$lt" : pricemax}},{"_id":0,"reviews":0})[page * nbel: page * nbel + nbel].sort("asin", 1)
        produits = collection.aggregate([
            {"$match":
                {
                    "categories": categorie,
                    "price": {"$gt": pricemin, "$lt": pricemax}
                }
            },
            {"$project":
                 {"_id": 0,
                  "asin": 1,
                  "groupe": {"$ifNull": [1, 1]},
                  "title": {"$ifNull": ["$title", "undefined"]},
                  "description": {"$ifNull": ["$description", "undefined"]},
                  "price": {"$ifNull": ["$price", "undefined"]},
                  "brand": {"$ifNull": ["$brand", "undefined"]},
                  "categories": {"$ifNull": ["$categories", "undefined"]},
                  "salesRank": {"$ifNull": ["$salesRank", "undefined"]},
                  "imUrl": {"$ifNull": ["$imUrl", "undefined"]}
                  }
             },
            {"$limit": page * nbel + nbel},
            {"$skip": page * nbel},
            {"$sort": {"asin": 1}}
        ])

    elif (categorie!='""' and title!='""'):
        #prixmax = collection.find({"title": {"$regex": title}},{"price":1}).sort([("price", -1)]).limit(1)
        count=collection.find({"categories": categorie, "price": {"$gt": pricemin, "$lt": pricemax}, "title": {"$regex": title}},{"_id":0}).count()
        #produits = collection.find({"categories": categorie, "price": { "$gt" :  pricemin, "$lt" : pricemax}, "title":{"$regex": title}},{"_id":0,"reviews":0})[page * nbel: page * nbel + nbel].sort("asin", 1)
        produits = collection.aggregate([
            {"$match":
                {
                    "categories": categorie,
                    "price": {"$gt": pricemin, "$lt": pricemax},
                    "title": {"$regex": title}
                }
            },
            {"$project":
                 {"_id": 0,
                  "asin": 1,
                  "groupe": {"$ifNull": [1, 1]},
                  "title": {"$ifNull": ["$title", "undefined"]},
                  "description": {"$ifNull": ["$description", "undefined"]},
                  "price": {"$ifNull": ["$price", "undefined"]},
                  "brand": {"$ifNull": ["$brand", "undefined"]},
                  "categories": {"$ifNull": ["$categories", "undefined"]},
                  "salesRank": {"$ifNull": ["$salesRank", "undefined"]},
                  "imUrl": {"$ifNull": ["$imUrl", "undefined"]}
                  }
             },
            {"$limit": page * nbel + nbel},
            {"$skip": page * nbel},
            {"$sort": {"asin": 1}}
        ])

    elif (categorie =='""'  and title != '""'):
        #prixmax = collection.find({"title": {"$regex": title}},{"price":1}).sort([("price", -1)]).limit(1)
        count=collection.find({"price": {"$gt": pricemin, "$lt": pricemax}, "title": {"$regex": title}},{"_id":0}).count()
        #produits = collection.find({"price": {"$gt": pricemin, "$lt": pricemax}, "title": {"$regex": title}},{"_id":0,"reviews":0})[page * nbel: page * nbel + nbel].sort("asin", 1)
        produits = collection.aggregate([
            {"$match":
                {
                    "price": {"$gt": pricemin, "$lt": pricemax},
                    "title": {"$regex": title}
                }
            },
            {"$project":
                 {"_id": 0,
                  "asin": 1,
                  "groupe": {"$ifNull": [1, 1]},
                  "title": {"$ifNull": ["$title", "undefined"]},
                  "description": {"$ifNull": ["$description", "undefined"]},
                  "price": {"$ifNull": ["$price", "undefined"]},
                  "brand": {"$ifNull": ["$brand", "undefined"]},
                  "categories": {"$ifNull": ["$categories", "undefined"]},
                  "salesRank": {"$ifNull": ["$salesRank", "undefined"]},
                  "imUrl": {"$ifNull": ["$imUrl", "undefined"]}
                  }
             },
            {"$limit": page * nbel + nbel},
            {"$skip": page * nbel},
            {"$sort": {"asin": 1}}
        ])

    r = count%nbelement
    nombrepage = ""

    if r > 0:
        nombrepage=str(count//nbelement + 1)
    else:
        nombrepage = str(count // nbelement)


    resultat = [produits, nombrepage, str(prixmaxresult)]
    return resultat

def get_mongo_produits_ids_in(collection, liste_ids_destination, range):
    produit = collection.aggregate([
        {"$match":
            {
                "asin": {"$in" : liste_ids_destination}
            }
        },
        {"$project":
             {"_id": 0,
              "asin": {"$ifNull": ["$asin", "undefined"]},
              "groupe": {"$ifNull": [range, range]},
              "title": {"$ifNull": ["$title", "undefined"]},
              "description": {"$ifNull": ["$description", "undefined"]},
              "price": {"$ifNull": ["$price", "0"]},
              "brand": {"$ifNull": ["$brand", "undefined"]},
              "categories": {"$ifNull": ["$categories", "undefined"]},
              "salesRank": {"$ifNull": ["$salesRank", "undefined"]},
              "imUrl": {"$ifNull": ["$imUrl", "undefined"]}
              }
         }
    ])
    result = []
    for p in produit:
        result.append(p)
    return result

def get_mongo_produit(collection,id):
    produit = collection.aggregate([
    {"$match":
     {
        "asin": id
     }
    },
    {"$project":
        { "_id" : 0,
          "asin": 1,
          "title": {"$ifNull": ["$title", "undefined"]},
          "description": {"$ifNull": ["$description", "undefined"]},
          "price": {"$ifNull": ["$price", "undefined"]},
          "brand": {"$ifNull": ["$brand", "undefined"]},
          "categories": { "$ifNull": ["$categories", "undefined"]},
          "salesRank": {"$ifNull": ["$salesRank", "undefined"]},
          "imUrl": {"$ifNull": ["$imUrl", "undefined"]},
          "reviews": { "$ifNull": ["$reviews", "undefined"]}
        }
    },
    {"$limit": 1}
    ])
    result = None
    for p in produit:
        result = p
    return result


def get_mongo_produit_source(collection,id):
    produit = collection.aggregate([
    {"$match":
     {
        "asin": id
     }
    },
    {"$project":
        { "_id" : 0,
          "asin": 1,
          "groupe": { "$ifNull": [1, 1]},
          "title": {"$ifNull": ["$title", "undefined"]},
          "description": {"$ifNull": ["$description", "undefined"]},
          "price": {"$ifNull": ["$price", "undefined"]},
          "brand": {"$ifNull": ["$brand", "undefined"]},
          "categories": { "$ifNull": ["$categories", "undefined"]},
          "salesRank": {"$ifNull": ["$salesRank", "undefined"]},
          "imUrl": {"$ifNull": ["$imUrl", "undefined"]},
          "reviews": { "$ifNull": ["$reviews", "undefined"]}
        }
    },
    {"$limit": 1}
    ])
    result = None
    for p in produit:
        result = p
    return result
