def get_id_produits(results, id):
    var = []
    var.append(id)
    for r in results:
        var.append(r['target'])
    return var

def formater_nodes(noeudsproduits):
    nodes =[]
    for p in noeudsproduits:
         nodes.append(p)
    return nodes

def get_distance(distance):
    var = []
    for d in distance:
        var.append(d['distance'])
    return var[0]


def convert_produits_to_json(produits):
    listeProduits = []

    if produits:
         for p in produits:
            listeProduits.append(p)
         return listeProduits
    else:
         return ("erreur dans la récupération des informations")


def convert_produit_to_json(produit):
    if produit:
        source=produit
        return source
    else:
        return ("erreur dans la récupération des informations")


def recherche_doublon(ids, relationniveau, relations ,listeIdFils):
    result =[]
    newlisteIdFils=[]
    for rn in relationniveau:
        doublon = False
        doublonS = False
        doublonF = False
        for rl in relations:
            if rn['source'] == rl['source'] and rn['target'] == rl['target'] and rn['link_type'] == rl['link_type']:
                doublon = True
                doublonS = True
                doublonF = True
                break
            if rn['source'] == rl['source'] or rn['source'] == rl['target'] :
                doublonS = True
            if rn['target'] == rl['target'] or rn['target'] == rl['source']:
                doublonF = True
        if doublon == False:
            relations = relations + [rn]
            if doublonS == False and doublonF == False:
                if rn['source'] != ids :
                    newlisteIdFils.append(rn['source'])
                    listeIdFils.append(rn['source'])
                if rn['target'] != ids:
                    newlisteIdFils.append(rn['target'])
                    listeIdFils.append(rn['target'])
            elif doublonF == False and doublonS == True:
                if rn['target'] != ids:
                    newlisteIdFils.append(rn['target'])
                    listeIdFils.append(rn['target'])
            elif doublonS == False and doublonF == True:
                if rn['source'] != ids:
                    newlisteIdFils.append(rn['source'])
                    listeIdFils.append(rn['source'])
        #s = " ids: " + str(ids) + "/ Rel" + str(relations) + " / idFils:" +str(listeIdFils)
        #print(s)
    result.append(relations)
    result.append(listeIdFils)
    result.append(newlisteIdFils)
    return result

def formater_relations(relations,nodes):
    listeRelationsFormatees = []
    for r in relations:
        cptS = 0
        cptF = 0

        sourcefound = False
        filsfound = False

        indexfils = None
        indexsource = None

        for n in nodes:
            if n['asin'] == r['source']:
                sourcefilsfound = True
                indexsource = cptS
            if n['asin'] == r['target']:
                filsfound = True
                indexfils = cptF
            #if sourcefound == True and filsfound == True:
            #    break
            cptF = cptF + 1
            cptS = cptS + 1
        listeRelationsFormatees.append(
            {
                'source': indexsource,
                'link_type': r['link_type'],
                'value': r['value'],
                'target': indexfils
            })
    return listeRelationsFormatees
