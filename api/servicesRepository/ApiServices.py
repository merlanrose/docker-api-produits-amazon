from flask import  make_response, jsonify

from api.queryRepository.MongoDbQueryRepository import get_mongo_categories,  \
     get_mongo_produits_ids_in, get_mongo_produits_recherche, get_mongo_produit, get_mongo_produit_source
from api.queryRepository.Neo4jQueryRepository import get_produit_source_fils_Neo4j, get_shortest_path_Neo4j, \
    get_produit_niveau_arbre_Neo4j
from api.servicesRepository.ServicesUtilitaires import get_id_produits, get_distance, \
     recherche_doublon, formater_relations

from api.servicesRepository.ServicesUtilitaires import convert_produits_to_json, formater_nodes

def get_produits_recherche_service(collection, numpage, nbelement,categorie,title,pricemin,pricemax):
    if (numpage < 1):
        return make_response(jsonify({'erreur': "le numeros de la page demandee est inferieur a 1 !"}), 404)
    if (nbelement < 1):
        return make_response(jsonify({'erreur': "le nombre d'elements demande est inferieur a 1 !"}), 404)
    if (pricemax!=None and pricemax<=pricemin):
        return make_response(jsonify({'erreur': "le prix minimum est superieur ou egal au prix maximal !"}), 404)
    produits = get_mongo_produits_recherche(collection,numpage,nbelement,categorie,title,pricemin,pricemax)
    listeproduits = convert_produits_to_json(produits[0])
    nbpage = produits[1]
    prixmax= produits[2]
    return make_response(jsonify({'nombrepage': nbpage, 'prixmax': prixmax, 'produits': listeproduits,
                                  'categories': get_mongo_categories(collection)}), 200)

def get_produit_source_fils_service(id, GRAPH_DATABASE,collection):
    results = get_produit_source_fils_Neo4j(id, GRAPH_DATABASE)
    liste_ids_destination = get_id_produits(results, id)
    # pas de noeuds fils
    if results.__len__() == 0:
        produit = get_mongo_produit(collection, id)
        # pas d'info sur le noeud ppére
        if produit == None:
            return make_response(jsonify({'erreur': "Pas de produit avec cet ID"}), 404)
        else:
            nodes = []
            nodes.append(produit)
            return make_response(
                jsonify({'nodes': nodes, 'links': []}), 200)
    #noeud fils présents
    else:
        relations=[]
        listeIdFils=[]
        nodes=[]
        relationniveau = get_produit_niveau_arbre_Neo4j(id, 0, GRAPH_DATABASE)
        results = recherche_doublon(id, relationniveau, relations, listeIdFils)
        relations = results[0]
        listeIdFils = results[1]
        #newlisteIdFils= results[3]

        noeudSource = get_mongo_produit_source(collection, id)
        noeudsproduits = get_mongo_produits_ids_in(collection, listeIdFils, 2)
        nodes.append(noeudSource)
        nodes = nodes + formater_nodes(noeudsproduits)


        listeRelationsFormatees = formater_relations(relations, nodes)

        return make_response(jsonify({'nodes': nodes, 'links': listeRelationsFormatees}), 200)
        #return make_response(jsonify({'nodes': nodes, 'links': relations}), 200)

def get_produit_niveau_arbre_service(idS, idD,GRAPH_DATABASE,collection):
    relations = []
    listeIdFils= []
    nodes= []
    if idS == idD: return get_produit_source_fils_service(idS,GRAPH_DATABASE,collection)
    else:
        distance = get_shortest_path_Neo4j(idS, idD, GRAPH_DATABASE)
        if distance.__len__() == 0:
            return make_response(jsonify({'erreur': "pas de chemin entre ces deux noeuds!"}), 404)
        else:
            noeudSource = get_mongo_produit_source(collection, idS)
            nodes.append(noeudSource)

            cpt = get_distance(distance) + 1
            for i in range(0, cpt):
                relationniveau = get_produit_niveau_arbre_Neo4j(idS, i, GRAPH_DATABASE)
                results = recherche_doublon(idS, relationniveau, relations ,listeIdFils)
                relations = results[0]
                listeIdFils =results[1]
                newlisteIdFils= results[2]

                if (i>0):
                    noeudsproduits = get_mongo_produits_ids_in(collection, newlisteIdFils, 3)
                else:
                    noeudsproduits = get_mongo_produits_ids_in(collection, newlisteIdFils, 2)

                nodes = nodes + formater_nodes(noeudsproduits)

    #noeudSource = get_mongo_produit_source(collection,idS)
    #nodes.append(noeudSource)

    #noeudsproduits = get_mongo_produits_ids_in(collection, listeIdFils)
    #nodes = formater_nodes(noeudSource,noeudsproduits)

    listeRelationsFormatees = formater_relations(relations,nodes)

    return make_response(jsonify({'nodes': nodes,'links': listeRelationsFormatees}),200)
    #return make_response(jsonify({'nodes': nodes,'links': relations}),200)

def get_categories_service(collection):
    categorie = get_mongo_categories(collection)
    return make_response(jsonify({'categories': categorie}), 200)

def get_produit_consultation(id,collection):
        produit = get_mongo_produit(collection, id)
        if produit == None:
            return make_response(jsonify({'erreur': "Pas de produit avec cet ID"}), 404)
        else:
            return make_response(
                jsonify(produit), 200)