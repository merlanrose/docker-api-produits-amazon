#Documentation API

( https://docs.google.com/document/d/1c8CE7AAnZWsrEPYPD49GItfYvLIh2qXmX_Stva0Qjto/edit?usp=sharing )

###Trois grandes fonctionnalités sont assurées par l’API et seront développées dans ce document. Nous détaillerons alors, le type de méthode, de données retournés, ainsi que leurs format. Mais ce document liste également les différents retours possibles de l’application (en scénario nominal ou d’échec).

+ Plan de test : https://docs.google.com/document/d/1YJO8lo_tJbF7jPhaAi8r73PZXLe_l5rpg4MznmJJfZk/edit?usp=sharing

+ Informations sur l'import des donnees : https://docs.google.com/document/d/1snvRRnswbGhfpTAdtvw6MGf5Eh2dn0n1TSIe_6z_m-M/edit?usp=sharing

*****

##Version du document :

+ ** Version** : V1.2

+ ** Auteur** :Naomie Fournié

+ ** Nature de la modification**: Maj read.me

+ ** Date de la modification**: 11/06/2018

*****

## Installation du projet:

+ git clone https://merlanrose@bitbucket.org/merlanrose/docker-api-produits-amazon.git

+ cd docker-api-produits-amazon

+ docker-compose build

+ docker-compose up

## Premièrement il faut configurer Neo4J

### 1. Se connecter au http://localhost:7474

### 2. Mettre en user neo4J et en passwort neo4J

### 3. Changer le password à : root

## Identifier les ids conteneurs Neo4J et db 

 + ouvrir un shell
 
 + docker ps

![dockerps](images/dockerps.png)


## Importer les données dans le conteneur MongoDb

### 1. Copier le fichier dans le répertoire data du projet 

+ docker cp <chemin-fichier>/docker-api-produits-amazon/data/produits.json idconteneur:/data/db/produits.json

### Pour exemple : 

+ docker cp Desktop/docker-api-produits-amazon/data/produits.json bca:/data/db/produits.json


### 2. Executer la commande d'import :

+ docker exec -it idconteneur mongoimport --db ppdprod --collection produit --type json --file "/data/db/produits.json"

### Pour exemple :

+ docker exec -it bca  mongoimport --db ppdprod --collection produit --type json --file "/data/db/produits.json"



## Importer les données dans le conteneur Neo4J

### 1. Copier le fichier dans le répertoire data du projet 

 + docker cp <chemin>/docker-api-produits-amazon/data/neo4jproduitslight.csv idconteneur:/var/lib/neo4j/import/neo4jproduitslight.csv

### Pour exemple : 

 + docker cp Desktop/docker-api-produits-amazon/data/neo4jproduitslight.csv 0d1:/var/lib/neo4j/import/neo4jproduitslight.csv
 
### 2. Se connecter au http://localhost:7474

+ Aller dans le browser

![bureauneo](images/bureauneo.png)

+ Passer les 3 commandes suivantes unitairement dans la barre de saisie:


+ La création d'un index 

create constraint on (produit:Produit) assert produit.id is unique


+ la création des noeuds


USING PERIODIC COMMIT 100000

LOAD CSV WITH HEADERS FROM "file:/neo4jproduitslight.csv" AS csvLine 

FIELDTERMINATOR ','

MERGE ( prodDest:Produit{id:(csvLine.IdDest)})

MERGE ( prodSource:Produit{id:(csvLine.IdSource)})



+ la création des relations


USING PERIODIC COMMIT 100000

LOAD CSV WITH HEADERS FROM "file:/neo4jproduitslight.csv" AS csvLine 

FIELDTERMINATOR ','

match ( prodSource:Produit { id: (csvLine.IdSource)}),(prodDest:Produit{id:(csvLine.IdDest)})

Create (prodSource)-[: relation { type : csvLine.TypeRelation, value: csvLine.ValRelation } ] ->(prodDest)


# Mainteneant vous pouvez vous connecter à votre localhost:5000 et tester l'api : 

 + http://127.0.0.1:5000/produits/numpage=3&nbelement=10&categorie=&title=la&pricemin=0&pricemax=1111
 
 + http://127.0.0.1:5000/consulter_produit/id_node=B000A88JZA
 
 + http://localhost:5000/ajouter_niveau_arbre/id_node_pere=B000A88JZA&id_node_fils=B00856XRLK


*****

## configuration BDD -> Modifiable dans api/init.py 

### Connection Mongo DB : 
 
 + 'MONGODB_DATABASE' = 'ppdprod'
 
 +  collection utilisee = produit

### Connection Neo4J

 + http://localhost:7474/db/data/
 
 + user='neo4j'
 
 + password='root'
 
 + node : Produits
 
 + relation de type SUBST ou COMPL
 

*****

##Sommaire des fonctionnalités
+ ### La consultation des produits selon des critères de recherche	
+ ### La consultation d’un produit via son id	
+ ### La consulter un arbre centré sur un produit donnée avec un niveau en plus

*****

#La consultation des produits selon des critères de recherche:

##Le système doit pouvoir retourner la liste des produits répondant à des critères de sélection et de pagination.

+ **Chemin de la fonction : ** '/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=<string:categorie>&title=<string:title>&pricemin=<int:pricemin>&pricemax=<int:pricemax>'

+ **Méthode de la fonction : **GET

+ **Paramètres de l’url : **
### 1. Premier paramètre de l’url :

+ **Définition fonctionnelle : **Le numéros de la page souhaitée pour consulter les produits

+ **Nom IT du paramètre : **numpage

+ **Type de la donnée :** int

+ **Pas de paramètres par défaut**

###2. Deuxième paramètre de l’url :
+ **Définition fonctionnelle : **Le nombre de produits affichés par page

+ **Nom IT du paramètre : **nbelement

+ **Type de la donnée :** int

+ **Pas de paramètres par défaut**

### 3. Troisième paramètre de l’url :
+ **Définition fonctionnelle :** La catégorie du produit souhaité

+ **Nom IT du paramètre : **categorie

+ **Type de la donnée :** string

+ **paramètre par défaut :** categorie="", pour ne pas effectuer de trie sur les produits avec la catégorie, la valeur de ”categorie” doit être valorisée avec cette valeur par défaut

### 4. Quatrième paramètre de l’url :

+ **Définition fonctionnelle :** Titre des produits, les produits retournés ayant ce motif  de caractère dans leur titre seront retournés

+ **Nom IT du paramètre :** titre

+ **Type de la donnée :** string

+ **paramètre par défaut :** titre="", pour ne pas effectuer de trie sur les produits avec un motif contenu dans le titre, la valeur de ”titre” doit être valorisée avec cette valeur par défaut


### 5. Sixième paramètre de l’url :
+ **Définition fonctionnelle :** Le prix minimal que les produits renvoyés doivent avoir

+ **Nom IT du paramètre :** pricemin

+ **Type de la donnée :** int

+ **Pas de paramètres par défaut**, mettre à 0

### 6. Septième paramètre de l’url :
+ **Définition fonctionnelle :** Le prix maximal que les produits renvoyés doivent avoir

+ **Nom IT du paramètre :** pricemax

+ **Type de la donnée :** int

+ **Pas de paramètres par défaut** : récupérer la valeur prix max de la requête

## Retour de l’application  en cas d’échec
**Si la valeur du numéros de la page est inférieur à 1**

+ **Le code de la réponse  : **404 NOT FOUND

+ **content-type: **application/json

+ **Les données retournées :** {"erreur": "le numéros de la page demandée est inférieur à 1 !"}

**Si le prix maximal est inférieur au prix minimum**

+ **Le code de la réponse  :** 404 NOT FOUND

+ **content-type: **application/json

+ **Les données retournées :** {'erreur': "le prix minimum est supérieur ou égal au prix maximal !"}

##Retour normal de l’application
+ **Le code le la réponse :** 200 OK

+ **content-type:** application/json

+ **Les données retournées :** le prix maximal dans la projection repondant aux critère, le nombre de page consutable d'élément répondant à ces critères, et un tableau de produits et de l’ensemble des catégories contenues dans la base MongoDB

+ "produits": [{},{}]

+ "categories": ["Dance","Other Sports","Sports & Outdoors"]

+ "prixmax": "3.17"

+ "nombrepage": "3"


##Exemple de données retournées :
+ **url utilisée :** http://localhost:8080/produits/numpage=4&nbelement=10&categorie=""&title=allet&pricemin=1&pricemax=4

+ **réponse : **

{

> "prixmax": "3.17"

> "nombrepage": "4"

> "categories": ["Dance","Other Sports","Sports & Outdoors"],

> "produits":

> [
>>{
>>
>>"asin": "9",
>>
>>"brand": "Coxlures",
>>
>>"categories": ["Sports & Outdoors","Other Sports","Dance"],
>>
>>"id": "9",
>>
>>"imUrl":"http://ecx.images._SY300_.jpg",
>>
>>"price": 3.17,
>>
>>"salesRank": {"Toys & Games": 211836},
>>
>>"title": "Girls Ballet Tutu Zebra Hot Pink"
>>
>>}
>
>]

}

*****

#La consultation d’un produit via son id:

##Le système doit pouvoir retourner les informations relative à un produit dont on connaît l’id, ainsi que des données sur les produits en relation avec, et les relations qui les lient.
+ **Chemin de la fonction :** '/consulter_produit/id_node=<string:id_node>

+ **Méthode de la fonction :** GET

+ **Paramètres de l’url**
### 1. Premier paramètre de l’url :
+ ** Définition fonctionnelle :** l’id du produit que l’on souhaite consulter
+ ** Nom IT du paramètre :** id
+ ** Type de la donnée :** string
+ ** Pas de paramètres par défaut**

### Retour de l’application  en cas d’échec
+ **Si l’id passé en paramètre n’est pas trouvé dans la base MongoDb**
+ **Le code de la réponse  :** 404 NOT FOUND
+ **content-type:** application/json
+ **Les données retournées : **{'erreur': "Pas de produit avec cet ID"}

### Retour normal de l’application
+ **Le code le la réponse :** 200 OK
+ **content-type:** application/json
+ **Les données retournées les différents champs du document qui compose ce produit

Exemple de retour  :  http://localhost:8080/consulter_produit/id_node=1

{

> "asin": "1",

> "brand": "Coxlures",

> "categories": ["Sports & Outdoors","Other Sports","Dance"],

> "id": "1",

> "imUrl": "http://ecx.i._SY300_.jpg",

> "price": 3.17,

> "salesRank": {"Toys & Games": 211836},

> "title": "Girls Ballet Tutu Zebra Hot Pink"

> "reviews":

>[{

>> "asin": "1",

>> "helpful": [2,3],

>> "overall": 5,

>> "reviewText": "review”,

>> "reviewerID": "A2SUAM1J3GNN3B",

>> "reviewerName": "J. McDonald",

>> "summary": "Heavenly Highway Hymns",

>> "unixReviewTime": 1252800000

>}]

}

*****

#La consultation d'un arbre de produit centré sur un produit et de ses fils à une profondeur définie

##Le système doit pouvoir retourner les informations relatives à un produit dont on connaît l’id, ainsi que les données sur les autres produits en relation avec ce dernier, et les relations qui les lient.
### Pour consulter les fils directs d'un noeud, il faut que le idsource donnés soit égal à l'idDestination.

+ **Chemin de la fonction :** /ajouter_niveau_arbre/id_node_pere=<string:idS>&id_node_fils=<string:idD>

+ **Méthode de la fonction :** GET

+ **Paramètres de l’url**
### 1. Premier paramètre de l’url :
+ **Définition fonctionnelle :** l’id du produit sur lequel l'arbre est centré
+ **Nom IT du paramètre : **idS (identifiant source)
+ **Type de la donnée : **string

### 2. Deuxième paramètre de l’url :
+ ** Définition fonctionnelle : **un id d' un produit en relation (directement ou indirectement ) avec l’id Source
+ ** Nom IT du paramètre : **idD (Destination)
+ ** Type de la donnée : **string
+ ** Si ce paramétre est égal au premier, seuls les produits ayant une relation directe avec le produit source seront retournés
+ ** Si un autre id en relation avec l'idsource est donnés, c'est ce dernier qui indiquera la profondeur de l'arbre a afficher. Le plus court chemin entre l'idsource et cette id destination sera caluculé puis incrémenté de 1.
Avec cette distance le système recherche toutes les relations directes et indirectes qui existent avec le noeud source jusqu'à cette profonceur.

## Un exemple :

Considérons l'arbre suivant :

![arbre](images/arbre.png)

Admettons que nous consultons le produit 1. qui est en relation directe avec les produits 2, 3, 4.

+ Si nous voulons connaitre les produits en relation direct avec 1 : Nous utiliserons /ajouter_niveau_arbre/1&1

+ Si nous souhaitons consulter le niveau suivant ( sur clique sur un des noeuds fils 2,3,4 via le front-end), nous utiliserons /ajouter_niveau_arbre/1&2 qui retournera les produits en relation avec les noeuds 2,3,4  (5,6,7,8) en plus des informations de /ajouter_niveau_arbre/1&1

+ Pour developper l'arbre de nouveau /ajouter_niveau_arbre/1&5 retournera en plus le noeud 9

+ Le noeud 10 n'est en relation avec aucun des produits, donc /ajouter_niveau_arbre/1&10 rettournera une erreur


### Retour de l’application  en cas d’échec
+ **Si aucun chemin n’est trouvé entre les deux noeuds**
+ **Le code de la réponse  :** 404 NOT FOUND
+ **content-type:** application/json
+ **Les données retournées : **{'erreur': "pas de chemin entre ces deux noeuds!"}

##Retour normal de l’application
+ **Le code le la réponse :** 200 OK
+ **content-type:** application/json
+ **Les données retournées : ** un tableau avec les information du noeud source (dont l’id est passé en paramètre), les noeuds destination (les noeuds qui sont des cibles d’une relation avec pour noeud source celui dont on connaît l’id) et les liens entre ces noeuds

## Exemple de données retournées :
http://localhost:8080/ajouter_niveau_arbre/9&9

+ Le Json a une structure semblable à la fontion précedente

{

>   "links": [

>    {

>>      "link_type": "COMPL",

>>      "link_value": "1",

>>      "source": "0",

>>      "target": "1"

>    }

>  ],

>  "nodes": [

>    {

>>      "asin": "9",

>>      "brand": "Coxlures",

>>      "categories": [

>>        "Sports & Outdoors",

>>        "Other Sports",

>>        "Dance"

>>      ],

>>      "id": "9",

>>      "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",

>>      "price": 3.17,

>>      "groupe" : 1,

>>      "salesRank": {

>>     "Toys & Games": 211836

>>      },

>>      "title": "Girls Ballet Tutu Zebra Hot Pink"

>>    },

>>    {

>>      "asin": "4",

>>      "brand": "Coxlures",

>>      "groupe" : 2

>>      "categories": [

>>        "Sports & Outdoors",

>>        "Other Sports",

>>        "Dance"

>>      ],

>>      "id": "4",

>>      "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",

>>      "price": 3.17,

>>      "salesRank": {

>>      "Toys & Games": 211836

>>      },

>>      "title": "Girls Ballet Tutu Zebra Hot Pink"

>    }

>  ]

}


