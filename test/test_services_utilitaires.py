import unittest
from py2neo import Graph
from api.servicesRepository.ServicesUtilitaires import get_distance,recherche_doublon
from api.queryRepository.Neo4jQueryRepository import get_produit_niveau_arbre_Neo4j,get_produit_source_fils_Neo4j,get_shortest_path_Neo4j

GRAPH_DATABASE = Graph("http://localhost:7474/db/data/",  user='neo4j', password='root')

class ServicesUtilsTest(unittest.TestCase):

    def tearDown(self):
        self.tearDownClass()

    def test_get_distance(self):
        result = get_shortest_path_Neo4j('B001F8TLLU', 'B004LE8TWM', GRAPH_DATABASE)
        distance = get_distance(result)
        self.assertEqual(distance, 1)

        result = get_shortest_path_Neo4j('B001F8TLLU', 'B004LE8UPS', GRAPH_DATABASE)
        distance = get_distance(result)
        self.assertEqual(distance, 2)

        result = get_shortest_path_Neo4j('B001F8TLLU', 'B002W5HN0G', GRAPH_DATABASE)
        distance = get_distance(result)
        self.assertEqual(distance, 3)

    def test_get_recherche_doublon(self):
        relations=[]
        listeIdFils=[]
        premier_niveau_arbre = get_produit_niveau_arbre_Neo4j('B001F8TLLU', 0 , GRAPH_DATABASE)
        self.assertEqual(len(premier_niveau_arbre), 5)

        #pas de relation encore dans la liste, elles doivent être toutes insérer
        resultat_recherhce = recherche_doublon('B001F8TLLU', premier_niveau_arbre , relations, listeIdFils)
        relations= resultat_recherhce[0]
        idFils=resultat_recherhce[1]
        self.assertEqual(len(listeIdFils), 4)
        self.assertEqual(len(relations), 5)

        self.assertEqual(idFils[0], "B004LE8TWM")
        self.assertEqual(idFils[1], "B004FN1ACK")
        self.assertEqual(idFils[2], "9729375011")
        self.assertEqual(idFils[3], "B000V5KPZ4")

        self.assertEqual(relations[0]['source'], "B001F8TLLU")
        self.assertEqual(relations[1]['source'], "B001F8TLLU")
        self.assertEqual(relations[2]['source'], "B001F8TLLU")
        self.assertEqual(relations[3]['source'], "B001F8TLLU")
        self.assertEqual(relations[4]['source'], "B001F8TLLU")

        self.assertEqual(relations[0]['link_type'], "SUBST")
        self.assertEqual(relations[1]['link_type'], "SUBST")
        self.assertEqual(relations[2]['link_type'], "SUBST")
        self.assertEqual(relations[3]['link_type'], "COMPL")
        self.assertEqual(relations[4]['link_type'], "COMPL")

        self.assertEqual(relations[0]['target'], "B004LE8TWM")
        self.assertEqual(relations[1]['target'], "B004FN1ACK")
        self.assertEqual(relations[2]['target'], "9729375011")
        self.assertEqual(relations[3]['target'], "B000V5KPZ4")
        self.assertEqual(relations[4]['target'], "9729375011")

        #ajout nouveux niveaux identique -> pas ajouts
        deuxieme_niveau_arbre = get_produit_niveau_arbre_Neo4j('B001F8TLLU', 1 , GRAPH_DATABASE)
        self.assertEqual(len(deuxieme_niveau_arbre), 24)
        resultat_recherhce = recherche_doublon('B001F8TLLU', deuxieme_niveau_arbre, relations, listeIdFils)
        relations = resultat_recherhce[0]
        listeIdFils = resultat_recherhce[1]
        self.assertEqual(len(listeIdFils), 9)
        self.assertEqual(len(relations), 24)

        self.assertEqual(relations[0]['source'], "B001F8TLLU")
        self.assertEqual(relations[1]['source'], "B001F8TLLU")
        self.assertEqual(relations[2]['source'], "B001F8TLLU")
        self.assertEqual(relations[3]['source'], "B001F8TLLU")
        self.assertEqual(relations[4]['source'], "B001F8TLLU")
        self.assertEqual(relations[5]['source'], "9729375011")
        self.assertEqual(relations[6]['source'], "9729375011")
        self.assertEqual(relations[7]['source'], "9729375011")
        self.assertEqual(relations[8]['source'], "9729375011")
        self.assertEqual(relations[9]['source'], "9729375011")
        self.assertEqual(relations[10]['source'], "B000V5KPZ4")
        self.assertEqual(relations[11]['source'], "B000V5KPZ4")
        self.assertEqual(relations[12]['source'], "B000V5KPZ4")
        self.assertEqual(relations[13]['source'], "B000V5KPZ4")
        self.assertEqual(relations[14]['source'], "B004FN1ACK")
        self.assertEqual(relations[15]['source'], "B004FN1ACK")
        self.assertEqual(relations[16]['source'], "B004FN1ACK")
        self.assertEqual(relations[17]['source'], "B004FN1ACK")
        self.assertEqual(relations[18]['source'], "B004FN1ACK")
        self.assertEqual(relations[19]['source'], "B004FN1ACK")
        self.assertEqual(relations[20]['source'], "B004LE8TWM")
        self.assertEqual(relations[21]['source'], "B004LE8TWM")
        self.assertEqual(relations[22]['source'], "B004LE8TWM")
        self.assertEqual(relations[23]['source'], "B004LE8TWM")

        self.assertEqual(relations[0]['link_type'], "SUBST")
        self.assertEqual(relations[1]['link_type'], "SUBST")
        self.assertEqual(relations[2]['link_type'], "SUBST")
        self.assertEqual(relations[3]['link_type'], "COMPL")
        self.assertEqual(relations[4]['link_type'], "COMPL")
        self.assertEqual(relations[5]['link_type'], "SUBST")
        self.assertEqual(relations[6]['link_type'], "SUBST")
        self.assertEqual(relations[7]['link_type'], "SUBST")
        self.assertEqual(relations[8]['link_type'], "COMPL")
        self.assertEqual(relations[9]['link_type'], "COMPL")
        self.assertEqual(relations[10]['link_type'], "SUBST")
        self.assertEqual(relations[11]['link_type'], "SUBST")
        self.assertEqual(relations[12]['link_type'], "COMPL")
        self.assertEqual(relations[13]['link_type'], "COMPL")
        self.assertEqual(relations[14]['link_type'], "SUBST")
        self.assertEqual(relations[15]['link_type'], "SUBST")
        self.assertEqual(relations[16]['link_type'], "SUBST")
        self.assertEqual(relations[17]['link_type'], "SUBST")
        self.assertEqual(relations[18]['link_type'], "COMPL")
        self.assertEqual(relations[19]['link_type'], "COMPL")
        self.assertEqual(relations[20]['link_type'], "COMPL")
        self.assertEqual(relations[21]['link_type'], "SUBST")
        self.assertEqual(relations[22]['link_type'], "SUBST")
        self.assertEqual(relations[23]['link_type'], "SUBST")

        self.assertEqual(relations[0]['target'], "B004LE8TWM")
        self.assertEqual(relations[1]['target'], "B004FN1ACK")
        self.assertEqual(relations[2]['target'], "9729375011")
        self.assertEqual(relations[3]['target'], "B000V5KPZ4")
        self.assertEqual(relations[4]['target'], "9729375011")
        self.assertEqual(relations[5]['target'], "B001F8TLLU")
        self.assertEqual(relations[6]['target'], "B004FN1ACK")
        self.assertEqual(relations[7]['target'], "097293751X")
        self.assertEqual(relations[8]['target'], "B000V5KPZ4")
        self.assertEqual(relations[9]['target'], "B001F8TLLU")
        self.assertEqual(relations[10]['target'], "B001F8TLLU")
        self.assertEqual(relations[11]['target'], "9729375011")
        self.assertEqual(relations[12]['target'], "9729375011")
        self.assertEqual(relations[13]['target'], "B001F8TLLU")
        self.assertEqual(relations[14]['target'], "B000V5KPZ4")
        self.assertEqual(relations[15]['target'], "B001F8TLLU")
        self.assertEqual(relations[16]['target'], "9729375011")
        self.assertEqual(relations[17]['target'], "097293751X")
        self.assertEqual(relations[18]['target'], "B001F8TLLU")
        self.assertEqual(relations[19]['target'], "B000V5KPZ4")
        self.assertEqual(relations[20]['target'], "B004LE8UPS")
        self.assertEqual(relations[21]['target'], "B00AYZFGZA")
        self.assertEqual(relations[22]['target'], "B001AL7GOQ")
        self.assertEqual(relations[23]['target'], "B004LE8TOA")


        # ['B004LE8TWM', 'B004FN1ACK', '9729375011', 'B000V5KPZ4']
        # ['B004LE8TWM', 'B004FN1ACK', '9729375011', 'B000V5KPZ4', '097293751X', 'B004LE8UPS', 'B00AYZFGZA', 'B001AL7GOQ', 'B004LE8TOA


if __name__ == '__main__':
    unittest.main()