import unittest
import json
from app import app

class MyAppCase(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

    def tearDown(self):
        self.tearDownClass()

    def test_categories(self):
        response = self.app.get("/categories")
        assert "200 OK" == response.status
        categories = json.loads(response.data)
        self.assertTrue(len(categories['categories']) == 1)
        self.assertEqual(categories['categories'][0], "Baby")


    def test_consulter_produits(self):
        response = self.app.get("consulter_produit/id_node=B001F8TLLU")
        assert "200 OK" == response.status
        data = json.loads(response.data)

        self.assertEqual(data['asin'], 'B001F8TLLU')
        self.assertEqual(len(data['reviews']) , 120)

        response = self.app.get("/consulter_produit/id_node=A#B@")
        produits = json.loads(response.data)
        assert "404 NOT FOUND" == response.status
        self.assertTrue(produits['erreur'] == "Pas de produit avec cet ID")

    def test_ajouter_niveau_arbre(self):
        response = self.app.get("/ajouter_niveau_arbre/id_node_pere=B001F8TLLU&id_node_fils=B001F8TLLU")
        relations = json.loads(response.data)
        assert "200 OK" == response.status

        self.assertEqual(len(relations['links']),5)
        self.assertEqual(len(relations['nodes']), 5)

        self.assertEqual(relations['links'][0]['source'], "0")
        self.assertEqual(relations['links'][1]['source'], "0")
        self.assertEqual(relations['links'][2]['source'], "0")
        self.assertEqual(relations['links'][3]['source'], "0")
        self.assertEqual(relations['links'][4]['source'], "0")

        self.assertEqual(relations['links'][0]['link_type'], "SUBST")
        self.assertEqual(relations['links'][1]['link_type'], "SUBST")
        self.assertEqual(relations['links'][2]['link_type'], "SUBST")
        self.assertEqual(relations['links'][3]['link_type'], "COMPL")
        self.assertEqual(relations['links'][4]['link_type'], "COMPL")

        self.assertEqual(relations['links'][0]['target'], "4")
        self.assertEqual(relations['links'][1]['target'], "3")
        self.assertEqual(relations['links'][2]['target'], "1")
        self.assertEqual(relations['links'][3]['target'], "2")
        self.assertEqual(relations['links'][4]['target'], "1")

        self.assertEqual(relations['nodes'][4]['target'], "B004LE8TWM")
        self.assertEqual(relations['nodes'][3]['target'], "B004FN1ACK")
        self.assertEqual(relations['nodes'][1]['target'], "9729375011")
        self.assertEqual(relations['nodes'][2]['target'], "B000V5KPZ4")
        self.assertEqual(relations['nodes'][0]['target'], "B001F8TLLU")

        #test cas KO : chemin entre les 2 noeuds inexistant
        response = self.app.get("/ajouter_niveau_arbre/id_node_pere=ABCV&id_node_fils=888B")
        produits = json.loads(response.data)
        assert "404 NOT FOUND" == response.status
        self.assertTrue(produits['erreur'] == "pas de chemin entre ces deux noeuds!")

        # test cas KO : chemin entre 1 neoud existant et un autre noeuds inexistant
        response = self.app.get("/ajouter_niveau_arbre/id_node_pere=B001F8TLLU&id_node_fils=B004LE8TWM")
        relations = json.loads(response.data)
        assert "200 OK" == response.status

        self.assertEqual(len(relations['links']), 24)
        self.assertEqual(len(relations['nodes']), 10)

        response = self.app.get("/ajouter_niveau_arbre/B001F8TLLU&150")
        produits = json.loads(response.data)
        assert "404 NOT FOUND" == response.status
        self.assertTrue(produits['erreur'] == "pas de chemin entre ces deux noeuds!")

    def test_consulation_produits(self):
        # test cas OK : chemin entre les 2 noeuds existant
        response = self.app.get("/produits/numpage=1&nbelement=10&categorie=\"\"&title=allet&pricemin=1&pricemax=4")
        relations = json.loads(response.data)
        assert "200 OK" == response.status

        self.assertEqual(relations['produits'][0]['asin'], "B00CDV4ZGE")
        self.assertEqual(relations['produits'][0]['title'],"Ballet Dress-Up Fairy Princess Tutu. Color: Black (Midnight) &quot;Little Mystique Legs&quot;")
        self.assertEqual(relations['produits'][1]['asin'], "B00CDV9MXU")
        self.assertEqual(relations['produits'][1]['title'], "Ballet Dress-Up Fairy Princess Tutu. Color: White (Snow) &quot;Little Mystique Legs&quot;")

        #test prix max selection + nb page retournées
        self.assertEqual(relations['prixmax'], '999.99')
        self.assertEqual(relations['nombrepage'], '1')

        response = self.app.get("/produits/numpage=2&nbelement=10&categorie=\"\"&title=allet&pricemin=1&pricemax=4")
        relations = json.loads(response.data)
        assert "200 OK" == response.status

        self.assertEqual(len(relations['produits']), 0)
        self.assertEqual(relations['categories'][0], "Baby")
        self.assertEqual(relations['prixmax'], '999.99')
        self.assertEqual(relations['nombrepage'], '1')


        response = self.app.get("/produits/numpage=3&nbelement=10&categorie=\"\"&title=al&pricemin=1&pricemax=10")
        relations = json.loads(response.data)
        self.assertEqual(len(relations['produits']), 10)
        assert "200 OK" == response.status

        self.assertEqual(relations['produits'][0]['asin'], "9881826764")
        self.assertEqual(relations['produits'][0]['title'], "Edu-Petit Happy Jungle Day Finger Puppet Book Developmental Toy")
        self.assertEqual(relations['produits'][1]['asin'], "9881826772")
        self.assertEqual(relations['produits'][1]['title'], "Edu-Petit My First Activity Crib Book Developmental Toy")
        self.assertEqual(relations['produits'][2]['asin'], "9896007497")
        self.assertEqual(relations['produits'][2]['title'],"Modern House Angry Bird I removable Vinyl Mural Art Wall Sticker Decal")
        self.assertEqual(relations['produits'][3]['asin'], "9896008124")
        self.assertEqual(relations['produits'][3]['title'],"Modern House Giraffe Children Height Measurement Growth Chart Vinyl Mural Art Wall Sticker Decal")
        self.assertEqual(relations['produits'][4]['asin'], "9896009783")
        self.assertEqual(relations['produits'][4]['title'],"Modern House Safari Animal Family removable Vinyl Mural Art Wall Sticker Decal")
        self.assertEqual(relations['produits'][5]['asin'], "B00004C8S8")
        self.assertEqual(relations['produits'][5]['title'], "Hospital's Choice Fold Up Nail Clippers")
        self.assertEqual(relations['produits'][6]['asin'], "B00004C8S9")
        self.assertEqual(relations['produits'][6]['title'], "Safety 1st Digital Pacifier Thermometer")
        self.assertEqual(relations['produits'][7]['asin'], "B000056HM5")
        self.assertEqual(relations['produits'][7]['title'],"Playtex Drop-Ins Original BPA Free Nurser Newborn Starter Set")
        self.assertEqual(relations['produits'][8]['asin'], "B000056OTF")
        self.assertEqual(relations['produits'][8]['title'],"Parent Units TV Guard Original Plastic Shield, Clear, 18 in.")
        self.assertEqual(relations['produits'][9]['asin'], "B000056W76")
        self.assertEqual(relations['produits'][9]['title'], "Dr. Browns 8oz. Natural Flow Standard Baby Bottle")

        response = self.app.get("/produits/numpage=4&nbelement=1&categorie=Baby&title=\"\"&pricemin=1&pricemax=4")
        data = json.loads(response.data)
        assert "200 OK" == response.status
        # test noms categorie
        self.assertEqual(data['categories'][0], "Baby")
        self.assertEqual(len(data['produits']), 1)
        self.assertEqual(data['produits'][0]['asin'], '8742248051')

        # test prix max selection + nb page retournées
        self.assertEqual(data['prixmax'], '999.99')
        self.assertEqual(data['nombrepage'], '1728')

        response = self.app.get("/produits/numpage=1&nbelement=10&categorie=Baby&title=allet&pricemin=1&pricemax=4")
        relations = json.loads(response.data)
        assert "200 OK" == response.status

        self.assertEqual(relations['produits'][0]['asin'], "B00CDV4ZGE")
        self.assertEqual(relations['produits'][0]['title'],"Ballet Dress-Up Fairy Princess Tutu. Color: Black (Midnight) &quot;Little Mystique Legs&quot;")
        self.assertEqual(relations['produits'][1]['asin'], "B00CDV9MXU")
        self.assertEqual(relations['produits'][1]['title'],"Ballet Dress-Up Fairy Princess Tutu. Color: White (Snow) &quot;Little Mystique Legs&quot;")

        # test prix max selection + nb page retournées
        self.assertEqual(relations['prixmax'], '999.99')
        self.assertEqual(relations['nombrepage'], '1')


    #http://localhost:8080/test/numpage=1&nbelement=10&categorie=DEFAULTPARAMCATEGORIE&title=allet&pricemin=1&pricemax=4
    #http://localhost:8080/test/numpage=1&nbelement=10&categorie=DEFAULTPARAMCATEGORIE&title=allet&pricemin=1&pricemax=4


if __name__ == '__main__':
    unittest.main()
