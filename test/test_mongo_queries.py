from unittest import TestCase
import mongomock
from mongomock import Database
from api.queryRepository.MongoDbQueryRepository import get_mongo_categories,get_mongo_produits_ids_in, get_mongo_produits_recherche, get_mongo_produit_source, get_mongo_produit

try:
    from bson.objectid import ObjectId
    import pymongo
    from pymongo import MongoClient as PymongoClient
    _HAVE_PYMONGO = True
except ImportError:
    from mongomock.object_id import ObjectId
    _HAVE_PYMONGO = False
try:
    from bson.code import Code
    from bson.son import SON
    import execjs  # noqa
    _HAVE_MAP_REDUCE = True
except ImportError:
    _HAVE_MAP_REDUCE = False



class DatabaseGettingTest(TestCase):

        def setUp(self):
            super(DatabaseGettingTest, self).setUp()
            self.client = mongomock.MongoClient()

        def test__getting_database_via_getattr(self):
            db1 = self.client.some_database_here
            db2 = self.client.some_database_here
            self.assertIs(db1, db2)
            self.assertIs(db1, self.client['some_database_here'])
            self.assertIsInstance(db1, Database)
            self.assertIs(db1.client, self.client)
            self.assertIs(db2.client, self.client)

        def test__getting_database_via_getitem(self):
            db1 = self.client['some_database_here']
            db2 = self.client['some_database_here']
            self.assertIs(db1, db2)
            self.assertIs(db1, self.client.some_database_here)
            self.assertIsInstance(db1, Database)

        def test__drop_database(self):
            db = self.client.produits
            collection = db.produits
            doc_id = collection.insert(
                { "id": "1",
                  "asin": "1",
                  "title": "Girls Ballet Tutu Zebra Hot Pink",
                  "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836},
                  "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports"],
                  "reviews":[{ "reviewerID": "A2SUAM1J3GNN3B",
                               "asin": "1",
                               "reviewerName": "J. McDonald",
                               "helpful": [2, 3],
                               "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                               "overall": 5.0,
                               "summary": "Heavenly Highway Hymns",
                               "unixReviewTime": 1252800000,
                               "reviewTime": "09 13, 2009"},
                             {"reviewerID": "A2SUAM1J3GNN3B",
                              "asin": "1",
                              "reviewerName": "J. McDonald",
                              "helpful": [2, 3],
                              "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                              "overall": 5.0,
                              "summary": "Heavenly Highway Hymns",
                              "unixReviewTime": 1252800000,
                              "reviewTime": "09 13, 2009" }]})
            result = collection.find({"_id": doc_id})
            self.assertEqual(result.count(), 1)

            self.client.drop_database("produits")
            result = collection.find({"_id": doc_id})
            self.assertEqual(result.count(), 0)

        def test__mongoQuerie_get_categories(self):
            db = self.client.produits
            collection = db.produits
            collection.insert_many(
                [{"id": "1", "asin": "1", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports", "Dance"]},
                 {"id": "2", "asin": "2", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports","Pouet pouet"]}
                 ])
            #Test nombre de categories distanctes
            result = get_mongo_categories(collection)
            self.assertEqual(len(result),4)


        def test__mongoQuerie_produits_id_in(self):
            db = self.client.produits
            collection = db.produits
            collection.insert_many(
                [{"id": "1", "asin": "1", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports"], "reviews": [
                        {"reviewerID": "A2SUAM1J3GNN3B", "asin": "1", "reviewerName": "J. McDonald", "helpful": [2, 3],
                         "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                         "overall": 5.0, "summary": "Heavenly Highway Hymns", "unixReviewTime": 1252800000,
                         "reviewTime": "09 13, 2009"},
                        {"reviewerID": "A2SUAM1J3GNN3B", "asin": "1", "reviewerName": "J. McDonald", "helpful": [2, 3],
                         "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                         "overall": 5.0, "summary": "Heavenly Highway Hymns", "unixReviewTime": 1252800000,
                         "reviewTime": "09 13, 2009"}]},
                 {"id": "2", "asin": "2", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports"], "reviews": [
                     {"reviewerID": "A2SUAM1J3GNN3B", "asin": "1", "reviewerName": "J. McDonald", "helpful": [2, 3],
                      "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                      "overall": 5.0, "summary": "Heavenly Highway Hymns", "unixReviewTime": 1252800000,
                      "reviewTime": "09 13, 2009"},
                     {"reviewerID": "A2SUAM1J3GNN3B", "asin": "1", "reviewerName": "J. McDonald", "helpful": [2, 3],
                      "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                      "overall": 5.0, "summary": "Heavenly Highway Hymns", "unixReviewTime": 1252800000,
                      "reviewTime": "09 13, 2009"}]},
                 {"id": "3", "asin": "3", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports", "Dance"], "reviews": [
                     {"reviewerID": "A2SUAM1J3GNN3B", "asin": "1", "reviewerName": "J. McDonald", "helpful": [2, 3],
                      "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                      "overall": 5.0, "summary": "Heavenly Highway Hymns", "unixReviewTime": 1252800000,
                      "reviewTime": "09 13, 2009"},
                     {"reviewerID": "A2SUAM1J3GNN3B", "asin": "1", "reviewerName": "J. McDonald", "helpful": [2, 3],
                      "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                      "overall": 5.0, "summary": "Heavenly Highway Hymns", "unixReviewTime": 1252800000,
                      "reviewTime": "09 13, 2009"}]},
                 {"id": "4", "asin": "4", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports", "Dance"], "reviews": [
                     {"reviewerID": "A2SUAM1J3GNN3B", "asin": "1", "reviewerName": "J. McDonald", "helpful": [2, 3],
                      "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                      "overall": 5.0, "summary": "Heavenly Highway Hymns", "unixReviewTime": 1252800000,
                      "reviewTime": "09 13, 2009"},
                     {"reviewerID": "A2SUAM1J3GNN3B", "asin": "1", "reviewerName": "J. McDonald", "helpful": [2, 3],
                      "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                      "overall": 5.0, "summary": "Heavenly Highway Hymns", "unixReviewTime": 1252800000,
                      "reviewTime": "09 13, 2009"}]},
                 {"id": "5", "asin": "5", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports", "Dance"], "reviews": [
                     {"reviewerID": "A2SUAM1J3GNN3B", "asin": "1", "reviewerName": "J. McDonald", "helpful": [2, 3],
                      "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                      "overall": 5.0, "summary": "Heavenly Highway Hymns", "unixReviewTime": 1252800000,
                      "reviewTime": "09 13, 2009"},
                     {"reviewerID": "A2SUAM1J3GNN3B", "asin": "1", "reviewerName": "J. McDonald", "helpful": [2, 3],
                      "reviewText": "I bought this for my husband who plays the piano. He is having a wonderful time playing these old hymns. The music is at times hard to read because we think the book was published for singing from more than playing from. Great purchase though!",
                      "overall": 5.0, "summary": "Heavenly Highway Hymns", "unixReviewTime": 1252800000,
                      "reviewTime": "09 13, 2009"}]}])

            #test le nombre de produits retourné pour une lise d'id en paramètre
            results = get_mongo_produits_ids_in(collection, ['1','2','3'],2)
            cpt = 0
            result=[]
            for r in results:
                cpt =cpt +1
                result.append(r)

            self.assertEqual(cpt,3)
            self.assertEqual(result[0]['asin'], '1')
            self.assertEqual(result[1]['asin'], '2')
            self.assertEqual(result[2]['asin'], '3')

            results = get_mongo_produits_ids_in(collection, ['2', '3'],2)
            result = []
            cpt = 0
            for r in results:
                cpt = cpt + 1
                result.append(r)
            self.assertEqual(cpt, 2)
            self.assertEqual(result[0]['asin'], '2')
            self.assertEqual(result[1]['asin'], '3')


        def test__mongoQuerie_get_mongo_produits_recherche(self):
            db = self.client.produits
            collection = db.produits
            collection.insert_many(
                [{"id": "1", "asin": "1", "title": "le chalet", "price": 1.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Maison", "Voyage","Construction"]},
                 {"id": "2", "asin": "2", "title": "la martinique", "price": 10.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Voyage", "Pays"]},
                 {"id": "3", "asin": "3", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports", "Dance","Livre"]},
                 {"id": "4", "asin": "4", "title": "le grand livre des comptes", "price": 5.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Compta", "Eco", "Livre"]},
                 {"id": "5", "asin": "5", "title": "les douze regles pour reussir sa vie", "price": 7.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Conseil", "Vie", "Dance","Livre"]}])

            #test Param par default param par default numpage=1, nbelement = 5
            numpage=1
            nbelement=5
            categorie ='\"\"'
            title ='\"\"'
            pricemin=0
            pricemax=10000
            result_cursor= get_mongo_produits_recherche(collection,numpage,nbelement, categorie,title,pricemin,pricemax)
            result=[]
            for r in result_cursor[0]:
                result.append(r)
            self.assertEqual(len(result), 5)
            self.assertEqual(result[0]['asin'], '1')
            self.assertEqual(result[1]['asin'], '2')
            self.assertEqual(result[2]['asin'], '3')
            self.assertEqual(result[3]['asin'], '4')
            self.assertEqual(result[4]['asin'], '5')
            self.assertEqual(result_cursor[2], "10.17")
            self.assertEqual(result_cursor[1], "1")

            # test Param par default param par default numpage=1, nbelement = 2
            numpage = 1
            nbelement = 2
            categorie = '\"\"'
            title = '\"\"'
            pricemin = 1
            pricemax = 11

            result_cursor = get_mongo_produits_recherche(collection, numpage, nbelement, categorie, title, pricemin,
                                                         pricemax)
            result = []
            for r in result_cursor[0]:
                result.append(r)
            self.assertEqual(len(result), 2)
            self.assertEqual(result[0]['asin'],'1')
            self.assertEqual(result[1]['asin'],'2')
            self.assertEqual(result_cursor[2], "10.17")
            self.assertEqual(result_cursor[1], "3")

            # test Param par default param par default numpage=2, nbelement = 2
            numpage = 2
            nbelement = 2
            categorie = '\"\"'
            title = '\"\"'
            pricemin = 1
            pricemax = 11
            result_cursor = get_mongo_produits_recherche(collection, numpage, nbelement, categorie, title, pricemin,
                                                         pricemax)
            result = []
            for r in result_cursor[0]:
                result.append(r)
            self.assertEqual(len(result), 2)
            self.assertEqual(result[0]['asin'], '3')
            self.assertEqual(result[1]['asin'], '4')
            self.assertEqual(result_cursor[2], "10.17")
            self.assertEqual(result_cursor[1], "3")


            # test Param avec categorie, titre par défault , numpage=1, nbelement = 3
            numpage = 1
            nbelement = 3
            categorie = 'Livre'
            title = '\"\"'
            pricemin = 3
            pricemax = 11

            result_cursor = get_mongo_produits_recherche(collection, numpage, nbelement, categorie, title, pricemin,
                                                         pricemax)
            result = []
            for r in result_cursor[0]:
                result.append(r)
            self.assertEqual(len(result), 3)

            self.assertEqual(result[0]['asin'], '3')
            self.assertEqual(result[1]['asin'], '4')
            self.assertEqual(result[2]['asin'], '5')
            self.assertEqual(result_cursor[2], "10.17")
            self.assertEqual(result_cursor[1], "1")


            # test Param avec categorie, titre  , numpage=1, nbelement = 3
            numpage = 1
            nbelement = 3
            categorie = 'Livre'
            title = 'al'
            pricemin = 3
            pricemax = 11
            result_cursor = get_mongo_produits_recherche(collection, numpage, nbelement, categorie, title, pricemin,
                                                         pricemax)
            result = []
            for r in result_cursor[0]:
                result.append(r)
            self.assertEqual(len(result), 1)

            self.assertEqual(result[0]['asin'], '3')
            self.assertEqual(result_cursor[2], "10.17")
            self.assertEqual(result_cursor[1], "1")

            # test Param par default param par default numpage=2, nbelement = 2
            numpage = 1
            nbelement = 5
            categorie = '\"\"'
            title = 'r'
            pricemin = 1
            pricemax = 11
            result_cursor = get_mongo_produits_recherche(collection, numpage, nbelement, categorie, title, pricemin,
                                                         pricemax)
            result = []
            for r in result_cursor[0]:
                result.append(r)
            self.assertEqual(len(result), 4)

            self.assertEqual(result[0]['asin'], '2')
            self.assertEqual(result[1]['asin'], '3')
            self.assertEqual(result[2]['asin'], '4')
            self.assertEqual(result[3]['asin'], '5')
            self.assertEqual(result_cursor[2], "10.17")
            self.assertEqual(result_cursor[1], "1")

            # test Param par default param par default numpage=2, nbelement = 2, primemin =5 et pricemax =11
            numpage = 1
            nbelement = 5
            categorie = '\"\"'
            title = 'r'
            pricemin = 5
            pricemax = 11
            result_cursor = get_mongo_produits_recherche(collection, numpage, nbelement, categorie, title, pricemin,
                                                         pricemax)
            result = []
            for r in result_cursor[0]:
                result.append(r)
            self.assertEqual(len(result), 3)

            self.assertEqual(result[0]['asin'], '2')
            self.assertEqual(result[1]['asin'], '4')
            self.assertEqual(result[2]['asin'], '5')
            self.assertEqual(result_cursor[2], "10.17")
            self.assertEqual(result_cursor[1], "1")

            # test Param par default param par default numpage=17, nbelement = 50, primemin =5 et pricemax =11
            numpage = 17
            nbelement = 50
            categorie = '\"\"'
            title = 'r'
            pricemin = 5
            pricemax = 11
            result_cursor = get_mongo_produits_recherche(collection, numpage, nbelement, categorie, title, pricemin,
                                                         pricemax)
            result = []
            for r in result_cursor[0]:
                result.append(r)
            self.assertEqual(len(result), 0)
            self.assertEqual(result_cursor[2], "10.17")
            self.assertEqual(result_cursor[1], "1")

        def test__mongoQuerie_get_produit(self):
            db = self.client.produits
            collection = db.produits
            collection.insert_many(
                [{"id": "1", "asin": "1", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports", "Dance"]},
                 {"id": "2", "asin": "2", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports","Pouet pouet"]}
                 ])

            result = get_mongo_produit(collection, "1")
            self.assertFalse(result== None)
            self.assertEqual(result['asin'],"1")

            result = get_mongo_produit(collection, "A")
            self.assertTrue(result == None)

        def test__mongoQuerie_get_produit_source(self):
            db = self.client.produits
            collection = db.produits
            collection.insert_many(
                [{"id": "1", "asin": "1", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports", "Dance"]},
                 {"id": "2", "asin": "2", "title": "Girls Ballet Tutu Zebra Hot Pink", "price": 3.17,
                  "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
                  "salesRank": {"Toys & Games": 211836}, "brand": "Coxlures",
                  "categories": ["Sports & Outdoors", "Other Sports","Pouet pouet"]}
                 ])

            result = get_mongo_produit_source(collection, "1")
            self.assertFalse(result== None)
            self.assertEqual(result['asin'],"1")
            self.assertEqual(str(result['groupe']), "1")

            result = get_mongo_produit_source(collection, "A")
            self.assertTrue(result == None)

