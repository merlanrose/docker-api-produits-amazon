import os
from py2neo import Graph
#from flask import jsonify, make_response
from api.servicesRepository.ApiServices import get_produit_source_fils_service, get_produit_niveau_arbre_service,get_produits_recherche_service, get_categories_service,get_produit_consultation
from flask_pymongo import PyMongo
from flask import Flask
from pymongo import MongoClient
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

client = MongoClient(
     os.environ['DB_PORT_27017_TCP_ADDR'],
     #'localhost',
     27017)
db = client.ppdprod
#GRAPH_DATABASE=Graph("http://localhost:7474/db/data/", bolt=False, user='neo4j', password='root')
GRAPH_DATABASE= Graph('http://'+ os.environ['NEO4J_PORT_7474_TCP_ADDR']+':7474/db/data',  bolt=False, user='neo4j', password='root')

#doit pouvoir prendre une url par default de type : http://127.0.0.1:5000/produits/numpage=1&nbelement=15&categorie=&title=&pricemin=null&pricemax=null
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=&title=<string:title>&pricemin=<int:pricemin>&pricemax=<int:pricemax>', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=<string:categorie>&title=&pricemin=<int:pricemin>&pricemax=<int:pricemax>', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=<string:categorie>&title=<string:title>&pricemin=null&pricemax=<int:pricemax>', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=<string:categorie>&title=<string:title>&pricemin=<int:pricemin>&pricemax=null', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=&title=&pricemin=<int:pricemin>&pricemax=<int:pricemax>', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=&title=<string:title>&pricemin=<int:pricemin>&pricemax=null', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=&title=<string:title>&pricemin=null&pricemax=<int:pricemax>', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=<string:categorie>&title=<string:title>&pricemin=null&pricemax=null', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=<string:categorie>&title=&pricemin=null&pricemax=<int:pricemax>', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=<string:categorie>&title=&pricemin=<int:pricemin>&pricemax=null', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=<string:categorie>&title=&pricemin=null&pricemax=null', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=&title=<string:title>&pricemin=null&pricemax=null', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=&title=&pricemin=null&pricemax=<int:pricemax>', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=&title=&pricemin=<int:pricemin>&pricemax=null', methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=&title=&pricemin=null&pricemax=null',methods=['GET'])
@app.route('/produits/numpage=<int:numpage>&nbelement=<int:nbelement>&categorie=<string:categorie>&title=<string:title>&pricemin=<int:pricemin>&pricemax=<int:pricemax>', methods=['GET'])
def get_test(numpage ,nbelement ,categorie='""',title='""',pricemin=0,pricemax=None):
    s = str(numpage) + str(nbelement) + str(categorie) + str(title) + str(pricemin) + str(pricemax)
    print(s)
    return get_produits_recherche_service(db.produit, numpage, nbelement,categorie,title,pricemin,pricemax)


# info produit + info produits fils + relation entre noeuds
# http://127.0.0.1:8080/consulter_produit/1
@app.route('/consulter_produit/id_node=<string:id_node>', methods=['GET'])
def get_produit_source_fils(id_node):
    return get_produit_consultation(id_node, db.produit)


# retourne les noeuds du niveau suivant de l'abre _ necessite le passage de l'id source (la tete de l'arbre) et l'id de noeud clique
# http://127.0.0.1:8080/ajouter_niveau_arbre/1&2
@app.route('/ajouter_niveau_arbre/id_node_pere=<string:idS>&id_node_fils=<string:idD>', methods=['GET'])
def get_produit_niveau_arbre(idS, idD):
    return get_produit_niveau_arbre_service(idS, idD, GRAPH_DATABASE, db.produit)

if __name__ == '__main__':
    #app.run(port=8080)
    app.run(host='0.0.0.0', debug=True)


@app.route('/categories', methods=['GET'])
def get_categories():
    return get_categories_service(db.produit)

# retorune la liste de l'ensemble des produits + liste des differentes categories de produits
# http://127.0.0.1:8080/produits
#@app.route('/produits', methods=['GET'])
#def liste_produits():
#    return liste_produits_service(db.produit)

# la liste des produits d'une categorie donnee dans l'url
# http://127.0.0.1:8080/produits_categorie/Dance
#@app.route('/produits_categorie/<string:categories>', methods=['GET'])
#def get_produits_categorie(categories):
#    return get_produits_categorie_service(categories, db.produit)

